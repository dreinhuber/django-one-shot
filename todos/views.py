from django.shortcuts import render
from django.views.generic.list import ListView
from todos.models import TodoList as TodoListModel

# Create your views here.
class TodoList(ListView):
    model = TodoListModel
